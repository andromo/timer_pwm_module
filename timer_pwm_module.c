#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/delay.h>
#include <asm-generic/uaccess.h>
#include <linux/gpio.h>


#include <asm/io.h>

/****************************************************************************/
/* Module licensing/description block.                                      */
/****************************************************************************/

MODULE_LICENSE("GPL");
MODULE_AUTHOR("DAC");
MODULE_DESCRIPTION("Simple module pwm driver on GPIO18");

/****************************************************************************/
// Globals
/****************************************************************************/

#define TIMER_BASE 	0x3f003000
#define PWM_BASE 	0x3f20C000
#define GPIO_BASE	0x3f200000
#define CLK_BASE 	0x3f101000

#define	BCM_PASSWORD	0x5A000000

#define TIMER_OFFSET 4  // lower 32 bits



#define GPFSEL0			(0x00/4)
#define GPFSEL1			(0x04/4)
#define GPSET0			(0x1c/4)
#define GPCLR0			(0x28/4)


// Port function select bits

#define	FSEL_INPT		0b000
#define	FSEL_OUTP		0b001
#define	FSEL_ALT0		0b100
#define	FSEL_ALT1		0b101
#define	FSEL_ALT2		0b110
#define	FSEL_ALT3		0b111
#define	FSEL_ALT4		0b011
#define	FSEL_ALT5		0b010

// PWM
//	Word offsets into the PWM control region

#define	PWM_CONTROL 0
#define	PWM_STATUS  1
#define	PWM0_RANGE  4
#define	PWM0_DATA   5
#define	PWM1_RANGE  8
#define	PWM1_DATA   9

//	Clock regsiter offsets

#define	PWMCLK_CNTL	40
#define	PWMCLK_DIV	41

#define	PWM0_MS_MODE    0x0080  // Run in MS mode
#define	PWM0_USEFIFO    0x0020  // Data from FIFO
#define	PWM0_REVPOLAR   0x0010  // Reverse polarity
#define	PWM0_OFFSTATE   0x0008  // Ouput Off state
#define	PWM0_REPEATFF   0x0004  // Repeat last value if FIFO empty
#define	PWM0_SERIAL     0x0002  // Run in serial mode
#define	PWM0_ENABLE     0x0001  // Channel Enable

#define	PWM1_MS_MODE    0x8000  // Run in MS mode
#define	PWM1_USEFIFO    0x2000  // Data from FIFO
#define	PWM1_REVPOLAR   0x1000  // Reverse polarity
#define	PWM1_OFFSTATE   0x0800  // Ouput Off state
#define	PWM1_REPEATFF   0x0400  // Repeat last value if FIFO empty
#define	PWM1_SERIAL     0x0200  // Run in serial mode
#define	PWM1_ENABLE     0x0100  // Channel Enable

// Timer
//	Word offsets

#define	TIMER_LOAD	(0x400 >> 2)
#define	TIMER_VALUE	(0x404 >> 2)
#define	TIMER_CONTROL	(0x408 >> 2)
#define	TIMER_IRQ_CLR	(0x40C >> 2)
#define	TIMER_IRQ_RAW	(0x410 >> 2)
#define	TIMER_IRQ_MASK	(0x414 >> 2)
#define	TIMER_RELOAD	(0x418 >> 2)
#define	TIMER_PRE_DIV	(0x41C >> 2)
#define	TIMER_COUNTER	(0x420 >> 2)


// PWM

#define	PWM_MODE_MS		0
#define	PWM_MODE_BAL	1


// gpioToPwmALT
//	the ALT value to put a GPIO pin into PWM mode

static uint8_t gpioToPwmALT [] =
{
          0,         0,         0,         0,         0,         0,         0,         0,	//  0 ->  7
          0,         0,         0,         0, FSEL_ALT0, FSEL_ALT0,         0,         0, 	//  8 -> 15
          0,         0, FSEL_ALT5, FSEL_ALT5,         0,         0,         0,         0, 	// 16 -> 23
          0,         0,         0,         0,         0,         0,         0,         0,	// 24 -> 31
          0,         0,         0,         0,         0,         0,         0,         0,	// 32 -> 39
  FSEL_ALT0, FSEL_ALT0,         0,         0,         0, FSEL_ALT0,         0,         0,	// 40 -> 47
          0,         0,         0,         0,         0,         0,         0,         0,	// 48 -> 55
          0,         0,         0,         0,         0,         0,         0,         0,	// 56 -> 63
} ;


// gpioToPwmPort
//	The port value to put a GPIO pin into PWM mode

static uint8_t gpioToPwmPort [] =
{
          0,         0,         0,         0,         0,         0,         0,         0,	//  0 ->  7
          0,         0,         0,         0, PWM0_DATA, PWM1_DATA,         0,         0, 	//  8 -> 15
          0,         0, PWM0_DATA, PWM1_DATA,         0,         0,         0,         0, 	// 16 -> 23
          0,         0,         0,         0,         0,         0,         0,         0,	// 24 -> 31
          0,         0,         0,         0,         0,         0,         0,         0,	// 32 -> 39
  PWM0_DATA, PWM1_DATA,         0,         0,         0, PWM1_DATA,         0,         0,	// 40 -> 47
          0,         0,         0,         0,         0,         0,         0,         0,	// 48 -> 55
          0,         0,         0,         0,         0,         0,         0,         0,	// 56 -> 63

} ;

// gpioToGPFSEL:
//	Map a BCM_GPIO pin to it's Function Selection
//	control port. (GPFSEL 0-5)
//	Groups of 10 - 3 bits per Function - 30 bits per port

static uint8_t gpioToGPFSEL [] =
{
  0,0,0,0,0,0,0,0,0,0,
  1,1,1,1,1,1,1,1,1,1,
  2,2,2,2,2,2,2,2,2,2,
  3,3,3,3,3,3,3,3,3,3,
  4,4,4,4,4,4,4,4,4,4,
  5,5,5,5,5,5,5,5,5,5,
} ;


// gpioToShift
//	Define the shift up for the 3 bits per pin in each GPFSEL port

static uint8_t gpioToShift [] =
{
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
} ;


void *timer_address;
void *pwm_address;
void *gpio_address;
void *clk_address;


static volatile uint32_t *gpio;
static volatile uint32_t *pwm;
static volatile uint32_t *clk;


static void pwmSetMode (int mode)
{
  if (mode == PWM_MODE_MS)
      *(pwm + PWM_CONTROL) = PWM0_ENABLE | PWM1_ENABLE | PWM0_MS_MODE | PWM1_MS_MODE ;
    else
*(pwm + PWM_CONTROL) = PWM0_ENABLE | PWM1_ENABLE ;
}

static void pwmSetRange (unsigned int range)
{
  *(pwm + PWM0_RANGE) = range ; udelay (10) ;
  *(pwm + PWM1_RANGE) = range ; udelay (10) ;
}

static void pwmStop(void )
{
	 *(pwm + PWM_CONTROL) = 0 ;
}


static void pwmStart(void )
{
	 *(pwm + PWM_CONTROL) = PWM0_ENABLE | PWM1_ENABLE | PWM0_MS_MODE | PWM1_MS_MODE ;
}

static void pwmSetClock (int divisor)
{
  uint32_t pwm_control ;
  divisor &= 4095 ;

  
    pwm_control = *(pwm + PWM_CONTROL) ;		// preserve PWM_CONTROL

// We need to stop PWM prior to stopping PWM clock in MS mode otherwise BUSY
// stays high.

    *(pwm + PWM_CONTROL) = 0 ;				// Stop PWM

// Stop PWM clock before changing divisor. The delay after this does need to
// this big (95uS occasionally fails, 100uS OK), it's almost as though the BUSY
// flag is not working properly in balanced mode. Without the delay when DIV is
// adjusted the clock sometimes switches to very slow, once slow further DIV
// adjustments do nothing and it's difficult to get out of this mode.

    *(clk + PWMCLK_CNTL) = BCM_PASSWORD | 0x01 ;	// Stop PWM Clock
      udelay (110) ;			// prevents clock going sloooow

    while ((*(clk + PWMCLK_CNTL) & 0x80) != 0)	// Wait for clock to be !BUSY
      udelay (1) ;

    *(clk + PWMCLK_DIV)  = BCM_PASSWORD | (divisor << 12) ;

    *(clk + PWMCLK_CNTL) = BCM_PASSWORD | 0x11 ;	// Start PWM clock
    *(pwm + PWM_CONTROL) = pwm_control ;		// restore PWM_CONTROL

}


static void pwmWrite (int pin, int value)
{
 
  *(pwm + gpioToPwmPort [pin]) = value ;
 
}


static void pwmToneWrite (int pin, int freq)
{
  int range ;

  if (freq == 0)
    pwmWrite (pin, 0) ;             // Off
  else
  {
    range = 600000 / freq ;
    pwmSetRange (range) ;
    pwmWrite    (pin, freq / 2) ;
  }
}


static void set_pwm_on(int pin)
{
	int fSel, shift, alt ;
	
	if ((alt = gpioToPwmALT [pin]) == 0)	// Not a hardware capable PWM pin
		return ;
	fSel    = gpioToGPFSEL [pin] ;
	shift = gpioToShift [pin] ;
	
	*(gpio + fSel) = (*(gpio + fSel) & ~(7 << shift)) | (alt << shift) ;
	udelay (110) ; 	
	pwmSetMode  (PWM_MODE_BAL) ;	// Pi default mode
    pwmSetRange (1024) ;		// Default range of 1024
	pwmSetClock (32) ; 		// 19.2 / 32 = 600KHz - Also starts the PWM
	pwmSetMode  (PWM_MODE_MS) ;	
}

static void usa_pwm(void)
{
	printk(KERN_NOTICE "mapping pwm @ 0x%p\n",(void *)(PWM_BASE));
	pwm_address = ioremap(PWM_BASE, 4096);
	pwm=(uint32_t *) pwm_address;
	printk(KERN_NOTICE "mapping gpio @ 0x%p\n",(void *)(GPIO_BASE));
	gpio_address = ioremap(GPIO_BASE, 4096);
	gpio= (uint32_t *) gpio_address;
	printk(KERN_NOTICE "mapping clk @ 0x%p\n",(void *)(CLK_BASE));
	clk_address = ioremap(CLK_BASE, 4096);
	clk= (uint32_t *) clk_address;
	
	set_pwm_on(18);
	set_pwm_on(18); // por qu� se activa a la segunda??
	pwmToneWrite(18, 440);
	msleep(100);
	pwmToneWrite(18, 1000);
	msleep(100);
	pwmToneWrite(18, 0);
}    


    
static void usa_timer(void)
{
	unsigned int tiempo1, tiempo2;
    printk(KERN_NOTICE "mapping timer @ 0x%p\n",(void *)(TIMER_BASE+TIMER_OFFSET));
    
	timer_address = ioremap(TIMER_BASE+TIMER_OFFSET, 4096);
	tiempo1=ioread32(timer_address);
	udelay(100);
	tiempo2=ioread32(timer_address);
	printk(KERN_NOTICE "timer value... %d\n", tiempo1);
	printk(KERN_NOTICE "udelay(100)\n");
	printk(KERN_NOTICE "timer value... %d\n", tiempo2);
	printk(KERN_NOTICE "diferencia en micro segundos: %d\n", (tiempo2-tiempo1<0)? tiempo2+0xFFFF-tiempo1 : tiempo2-tiempo1);
}	

/****************************************************************************/
/* Module init / cleanup block.                                             */
/****************************************************************************/
int r_init(void) 
{	
    printk(KERN_NOTICE "Hello, loading DSO %s module!\n",KBUILD_MODNAME);
    
	usa_timer();
	
	usa_pwm();
    
    return 0;
}

void r_cleanup(void) 
{	
    printk(KERN_NOTICE "DSO %s module cleaning up...\n",KBUILD_MODNAME);
    
    printk(KERN_NOTICE "unmapping...\n");
    
    iounmap(timer_address);
    iounmap(pwm_address);
    iounmap(gpio_address);
    iounmap(clk_address);
   
	printk(KERN_NOTICE "Done. Bye\n");
    return;
}

module_init(r_init);
module_exit(r_cleanup);
